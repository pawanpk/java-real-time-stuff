package com.pawam.files;

public class NotEligibleException extends Exception{

	public NotEligibleException() {
		super();
	}
	
	public NotEligibleException(String message){
		super(message);
	}
	
}
