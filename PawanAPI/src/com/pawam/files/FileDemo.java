package com.pawam.files;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.Scanner;

public class FileDemo {

	public static void main(String[] args) throws FileNotFoundException, IOException {
		String filePath = "F:\\files\\abc.txt";
		
		System.out.println(filePath);
		
		File file = new File(filePath);
		
//		readDataUsingBufferedReader(file);
//		readDataUsingScanner(file);
//		readDataUsingRandomAccessFile(file);
		
		String data = "New line..2";
		
		FileWriter fw = null;
		
		try {
			fw = new FileWriter(file, true);
			fw.write(data);
			fw.flush();
			
		}catch (IOException e) {
			e.printStackTrace();
		}finally {
			fw.close();
		}
		
		
	}
	
	public static void readDataUsingBufferedReader(File file) throws IOException {
		
		FileReader fReader = new FileReader(file);
		
		BufferedReader bReader = new BufferedReader(fReader);
		
		String line = null;
		
		while((line = bReader.readLine()) != null) {
			System.out.println(line);
		}
		
		fReader.close();
		bReader.close();
		
		
	}
	
	public static void readDataUsingScanner(File file) throws FileNotFoundException {
		
		Scanner scanner = new Scanner(file);
		
		String line = null;
		
		while(scanner.hasNextLine()) {
			line = scanner.nextLine();
			
			System.out.println(line);
		}
		
		scanner.close();
		
	}
	
	public static void readDataUsingRandomAccessFile(File file) throws IOException {
		
		RandomAccessFile raf = new RandomAccessFile(file, "r");
		
		String line = null;
		
		while((line = raf.readLine()) != null) {
			
			System.out.println(line);
		}
		
		raf.close();
		
	}

}
