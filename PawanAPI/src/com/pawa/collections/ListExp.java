package com.pawa.collections;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ListExp {

	public static void main(String[] args) {
		
		List<String> list = new ArrayList<String>();
		list.add("A");
		list.add("B");
		list.add("A");
		list.add("C");
		list.add("D");
		list.add(null);
		list.add("D");
		
		System.out.println(list);
		
		Iterator<String> itr = list.iterator();
		
		while(itr.hasNext()) {
//			System.out.println(itr.next());
			if(itr.next() == null)
				itr.remove();
		}
		
		System.out.println(list);
		
		
	}

}
